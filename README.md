# generator

#### 介绍
桌面版代码生成器及jfinal enjoy 和db的用法的练习小工具，采用jfinal enjoy engine进行模板渲染，采用tornadofx进行界面开发。也可用于练习jfinal enjoy模板引擎的用法。

#### [操作演示](https://gitee.com/y2h/generator/blob/master/doc/%E6%A1%8C%E9%9D%A2%E7%89%88%E4%BB%A3%E7%A0%81%E7%94%9F%E6%88%90%E5%99%A80.0.3%E7%89%88jfinal%20db%20record%20%E6%BC%94%E7%A4%BA.gif)
https://www.bilibili.com/video/av54640136
https://www.bilibili.com/video/av54639339
![操作演示](https://gitee.com/y2h/generator/blob/master/doc/%E6%A1%8C%E9%9D%A2%E7%89%88%E4%BB%A3%E7%A0%81%E7%94%9F%E6%88%90%E5%99%A80.0.3%E7%89%88jfinal%20db%20record%20%E6%BC%94%E7%A4%BA.gif)
#### 软件架构
1. jdk8
2. kotlin, [tornadofx](https://tornadofx.io/)
3. [jfinal](https://www.jfinal.com) [enjoy](https://www.jfinal.com)
4. slqite, mysql, postgresql
5. [hutool](https://hutool.cn/)


#### 安装教程

1. 配置好java8环境
2. 采用idea开发，配置好kotlin和tornadofx环境
3. 在idea中导入为gradle工程

#### 使用说明
下载编译好的文件[generator.jar](https://gitee.com/y2h/generator/attach_files/241900/download)
1. 开启mysql, postgresql,sqlite等数据库服务，并建立好数据库
2. 运行软件：java -jar generator.jar，选择数据库类型，测试能否连接成功
3. 连接成功后，选择需要生成代码的数据表

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
