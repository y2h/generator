
![输入图片说明](https://gitee.com/y2h/generator/blob/sqlite/doc/learn_jfinal_engine.png "在这里输入图片标题")
生成可执行jar
export JAVA_HOME=/soft/sss/jdk8
gradle jfxJar

获取mysql中指定库的所有表名
select table_name from information_schema.TABLES where TABLE_SCHEMA='db_name'


PostgreSQL获取所有数据库名
 SELECT datname FROM pg_database;
 
Postgresql 如何查出一个表的所有列名
select * from information_schema.columns where table_schema='public' and table_name='md_qq';

PostgreSQL获取当前数据库中所有table名
SELECT tablename FROM pg_tables WHERE tablename NOT LIKE 'pg%' AND tablename NOT LIKE 'sql_%' ORDER BY tablename