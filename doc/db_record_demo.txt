#(jDbPro)
#set(jdb=jDbPro)
#set(tbl="user")
#set(sql="select * from "+tbl)
#(sql)
#(	jdb.find(sql)	)

#for(i:[1..30])
  #(record.clear() ?? "record is not exist")
  #set(    user = record.set("name", "James"+i).set("age", i)  ) 
  #(user)
  #(jdb.save(tbl,user))
#end

#set(  	sql_delete="delete from "+tbl	)
#(	jdb.delete(sql_delete)		)

#( jdb.findAll(tbl)	)

#set(    user = record.set("name", "James"+2111).set("age", 2111)  ) 
  #(user)
#(	user.getStr("name")		)
#(	user.getInt("age")	)
  #(jdb.save(tbl,user))

#(	jdb.deleteById("user", 33)	)
#set(user2=jdb.findById("user", 2).set("name", "James22") 	)
#(	user2		)
#set(sql_update="update user set age=111 where id='10'")
#(	jdb.update(sql_update	)	)
#(	jdb.findById(tbl,10	)	)

#set( users = jdb.find("select * from user where age > 18")	)
#(users)

#set( userPage = jdb.paginate(1, 10, "select *", "from user where age > ?", 18)	)
#(userPage)

#set(sql_int="select age from "+tbl	)
#(	jdb.queryInt(sql_int)		)

#set(sql_string="select name from "+tbl	)
#(	jdb.queryStr(sql_string)		)

#set(sql_string="select name from "+tbl	)
#(	jdb.query(sql_string)		)

#(	jdb.queryFirst(sql)	)

