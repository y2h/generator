### 0.0.6
1. 升级依赖
2. 使用建造者模式封闭不同数据库类型的属性
3. 当切换不同的数据库类型时，清空gmodel.dbs，gmodel.tables
```
gmodel.dbs.clear()
gmodel.tables.clear()
```

### 0.0.5
1. 当切换数据库时，关闭之前的ActiveRecordPlug,即gmode.arp.value.stop()
2. 将Kv添加到共享对象
```
addSharedObject("kv", Kv())
```
3. 增加SQL 模板功能gmodel.arp.value.addSqlTemplate("all.sql");
2. 设置打开对话框的初始目录为当前项目根目录
```
val efset = arrayOf(FileChooser.ExtensionFilter("选择模板文件", "*.*"))
val fnset = chooseFile("选择模板文件", efset, FileChooserMode.Single){
    File("").getCanonicalPath()
    // 初始目录为当前项目目录
    initialDirectory=File(File("").getCanonicalPath())
}
```
3. 将数据库列表框的选择事件的代码提取成方法genSrv.prapare()，当数据库切换时，得到所选择数据库中的所有表，并将新的jDbPro对象加入到engine的SharedObjectMap中。

### 0.0.4
1. 改进dbst和tables的清空时机，优化体验
2. 可随意更换数据库而不用重新启动程序，前端模板中代表数据库连接的name，即“jDbPro"保持不变
3. 程序退出时关闭ActiveRecordPlug,即gmode.arp.value.stop()

建议engine 或EngineConfig中再增加一个的方法

建议engine或EngineConfig中再重载一个新的addSharedObject方法，如下：

public Engine addSharedObject(String name, Object object，boolean b) {
 当b=true时，如果当前sharedObjectMap中存在名称为name的sharedObject，则保持name不变，更新sharedObject
 return this;
}
目的：保持前端模板引擎中的name不变，但其所引用的对象可以根据需要更新。

例如：[代码生成器](https://www.jfinal.com/project/290) 前端模板字符串中#(jDbPro)保持不变，后端当更换数据库连接时，动态更新其所代表的数据库连接对象。

借助enjoy模板引擎，前端可以使用java的任意类和对象及其方法，让java可以前后端通吃


### 0.0.3
1. 调整engine 中的gmodel、cn.hutool.db.Db、cn.hutool.json.JSONUtil等三个SharedObject到gmodel的init方法中[Gmodel.kt](https://gitee.com/y2h/generator/tree/master/src/main/kotlin/com/cyy/model/Gmodel.kt)
2. engine 中增加com.jfinal.plugin.activerecord.Db、com.jfinal.plugin.activerecord.Record两个SharedObject，用来练习jfinal的Db + Record模式[Gmodel.kt](https://gitee.com/y2h/generator/tree/master/src/main/kotlin/com/cyy/model/Gmodel.kt)
3. 调整布局，增加"渲染选择的模板字符串"和"清空模板缓存"2个按钮，方便练习engine用法[GEnjoy](https://gitee.com/y2h/generator/tree/master/src/main/kotlin/com/cyy/view/GEnjoy.kt)
4. 在模板字符串“模板字符串”文本框中增加“渲染选中”右键菜单[GEnjoy](https://gitee.com/y2h/generator/tree/master/src/main/kotlin/com/cyy/view/GEnjoy.kt)
5. 每次测试数据库连接时，关闭上次测试时创建的数据库连接，程度退出时关闭最后一次创建的数据库连接[TopView.kt](https://gitee.com/y2h/generator/blob/master/src/main/kotlin/com/cyy/view/TopView.kt)
6. 通过engine.addSharedObject("jDbPro", jDbPro)，每次测试数据库连接时，都将最新的有效的DbPro添加到EngineConfig的sharedObjectMap中去，实现不生重启程序也可以切换数据库进行相关操作。hutool db由于可以在模板中设置datasource，所以只需在gmodel初始化时添加一次到sharedObjectMap中去就可以了
7. 多次运行engine.addSharedObject("jDbPro", jDbPro)时，会抛异常。在Constants类中增加addSharedObject方法，确保每次engine.addSharedObject("jDbPro", jDbPro)中的"jDbPro"保持不变，从而当数据库连接发生变化时，模板中就不用每次都更换SharedObject的名称
8. 由于jfinal EngineCoinfig中sharedObjectMap是私有的，只好通过反射，有数据库连接发生变化时，对其进行重新赋值。具体实现见TopView.kt中的测试连接按钮的action代码[TopView.kt](https://gitee.com/y2h/generator/blob/master/src/main/kotlin/com/cyy/view/TopView.kt)
9. 2019/06/09 用了一天时间，一边学习java中的反射知识，一边实现上述功能。
10. 为了独立使用jfinal activeRecord时更加方便灵活和可配置，波总是否考虑暴露或提供更多属性和方法，方便其它场景下使用。

### 0.0.2
1. engine 中增加gmodel、cn.hutool.db.Db、cn.hutool.json.JSONUtil等三个SharedObject，可以用来练习jfinal db的使用方法
2. 调整布局，单独增加一个tab用来练习jfina enjoy 和 db的使用方法
