package com.cyy.view

import cn.hutool.core.io.FileUtil
import com.cyy.model.GmodelModel
import javafx.geometry.Orientation
import javafx.stage.FileChooser
import tornadofx.*

/**
 * 设置生成代码所在目录
 */
class DirSetView(val gmodel:GmodelModel) : View("目录配置") {
    override val root = vbox {
        form() {
            fieldset("生成项目目录",labelPosition = Orientation.VERTICAL) {

                field("模板文件根目录") {
                    button("...") {
                        action {
                            try {
                                val fn = chooseDirectory("模板文件根目录", FileUtil.getUserHomeDir()) {
                                }
                                if (fn != null) {
                                    gmodel.baseTemplatePath.value = "${fn.absolutePath}"

                                }
                                gmodel.baseTemplatePath.value = "template"
                            } catch (e: Exception) {
                                println(e.toString())
                                return@action
                            }
                        }
                    }
                    textfield(gmodel.baseTemplatePath) {
                        text = "generator"
                        required(message = "Enter user 模板文件根目录")
                    }
                }

                field("选择配置文件") {
                    button("...") {
                        action {
                            val efset = arrayOf(FileChooser.ExtensionFilter("选择配置文件", "*.*"))

                            val fnset = chooseFile("选择配置文件", efset, FileChooserMode.Single)
                            if (fnset.isNotEmpty()) {
                                gmodel.settingFile.value = "${fnset.first()}"
                            }
                        }
                    }
                    textfield(gmodel.settingFile) {
                        text = "generator/generator.properties"
                    }
                }
                field("projectName") {
                    textfield(gmodel.projectName) {
                        text="jxtpro"
                    }
                }
                field("baseSrcPath") {
                    textfield(gmodel.baseSrcPath) {
                        text="src/main/java/"
                    }
                }
                field("baseWebAppPath") {
                    textfield(gmodel.baseWebAppPath) {
                        text="src/main/webapp/"
                    }
                }
                field("baseResourcesPath") {
                    textfield(gmodel.baseResourcesPath) {
                        text="src/main/resources/"
                    }
                }
                field("baseWEBINFPath") {
                    textfield(gmodel.baseWEBINFPath) {
                        text="src/main/webapp/WEB-INF/"
                    }
                }

                field("projectPackage") {
                    textfield(gmodel.projectPackage) {
                        text="com.jxtpro.demo"
                    }
                }
            }
        }

    }
}
