package com.cyy.view

import com.cyy.model.GmodelModel
import com.cyy.srv.GenSrv
import tornadofx.*

class MainTap(val gmodel: GmodelModel, val genSrv: GenSrv) : View("My View") {

    override val root = tabpane {
        tab("强大生成器") {
            isClosable=false
            add(GEnjoy(gmodel,genSrv))
        }
        tab("数据库配置") {
            isClosable=false
            add(DBView(gmodel, genSrv))
        }
        tab("目录配置") {
            isClosable=false
            add(DirSetView(gmodel))
        }
        tab("Kv") {
            isClosable=false
            add(SsetView(gmodel))
        }
//        tab("配置文件1") {
//            isClosable=false
//            add(SetFileView(gmodel))
//        }
    }
}
