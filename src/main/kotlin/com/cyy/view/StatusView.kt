package com.cyy.view

import com.cyy.model.GmodelModel
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Orientation
import javafx.scene.layout.Priority
import tornadofx.*

class StatusView(val gmodel: GmodelModel) : View() {

    val vm : StatusViewModel by inject()

    override val root = hbox(10){
        label("[db Type]:")
        label(gmodel.dbtype)
        separator(Orientation.VERTICAL)
        label("[db Port]:")
        label(gmodel.port)
        separator(Orientation.VERTICAL)
        label("[db File]:")
        label(gmodel.dbname)
        separator(Orientation.VERTICAL)
        label("[db Driver]:")
        label(gmodel.driver)
        separator(Orientation.VERTICAL)
        label(gmodel.driver)
        separator(Orientation.VERTICAL)
        label("看我："){

        }

        label(vm.lastMessage)
        spacer()

        paddingAll = 4
        vgrow = Priority.NEVER
        prefHeight=10.0
    }
}

class StatusViewModel : ViewModel() {

    val lastMessage = SimpleStringProperty()

    init {
        subscribe<GenEvent> {
            lastMessage.value = it.message
        }
    }
}