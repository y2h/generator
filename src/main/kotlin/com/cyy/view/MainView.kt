package com.cyy.view

import com.cyy.model.GmodelModel
import com.cyy.srv.GenSrv
import javafx.scene.layout.Priority
import tornadofx.*

class MainView : View("Generator代码生成器,采用jfinal enjoy模板引擎") {
    //    val form : FormView by inject()
//    val status : StatusView by inject()
    val genSrv: GenSrv by inject()
    val gmodel: GmodelModel by inject()

    override val root = borderpane {
        //        left<DBView>()
        center{
            add(MainTap(gmodel,genSrv))
        }

        top {
            add(TopView(gmodel, genSrv))
        }
        bottom{
            add(StatusView(gmodel))
        }

        //        add(form)
//        separator()
//        add(status)
//        top<TopView>()

        prefWidth = 1000.0
        prefHeight = 700.0
        paddingAll = 10
        vgrow = Priority.ALWAYS
        hgrow=Priority.ALWAYS
//        alignment = Pos.CENTER
    }
}

class GenEvent(val message: String) : FXEvent()