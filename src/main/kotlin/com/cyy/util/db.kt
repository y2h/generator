package com.cyy.util

import com.cyy.view.Setts
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.transactions.TransactionManager
import java.sql.Connection

private var LOG_TO_CONSOLE: Boolean = false

//fun newTransaction(): Transaction = TransactionManager.currentOrNew(TRANSACTION_SERIALIZABLE).apply { logger.addLogger(StdOutSqlLogger) }
fun newTransaction(): Transaction = TransactionManager.currentOrNew(Connection.TRANSACTION_SERIALIZABLE).apply {
    if (LOG_TO_CONSOLE) addLogger(StdOutSqlLogger)
}
// Isolation level options: TRANSACTION_SERIALIZABLE, TRANSACTION_READ_UNCOMMITTED

fun enableConoleLogger() {
    LOG_TO_CONSOLE = true
}

fun createTables() {
    with(newTransaction()) {
        SchemaUtils.create(Setts)
    }
}

fun <T> execute(command: () -> T) : T {
    with (newTransaction()) {
        return command().apply {
            commit()
            close()
        }
    }
}