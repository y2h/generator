package com.cyy.app

import com.cyy.model.Constants
import com.cyy.model.GmodelModel
import com.cyy.view.MainView
import tornadofx.App

class MyApp : App(MainView::class, Styles::class) {
    val gmodel: GmodelModel by inject()

    override fun stop() {
        try {
            Constants.closeDb(gmodel.dataSource.value)
            gmodel.arp.value.stop()
        } catch (e: Exception) {
            println("some error happen when app stop : ${e}")
        }

        super.stop()
    }
}
