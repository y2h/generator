package com.cyy.model

import cn.hutool.json.JSONUtil
import com.jfinal.kit.Kv
import com.jfinal.kit.StrKit
import com.jfinal.plugin.activerecord.ActiveRecordPlugin
import com.jfinal.plugin.activerecord.Config
import com.jfinal.plugin.activerecord.Db
import com.jfinal.plugin.activerecord.dialect.Dialect
import com.jfinal.plugin.activerecord.generator.MetaBuilder
import com.jfinal.template.Engine
import javafx.collections.FXCollections
import tornadofx.*
import javax.json.JsonObject
import javax.sql.DataSource

//数据库模型　JsonModel
class Gmodel : JsonModel {
    val bmpkgProperty = stringProperty()
    var bmpkg: String by bmpkgProperty

    val bmpkgpathProperty = stringProperty()
    var bmpkgpath: String? by bmpkgpathProperty

    val mpkgProperty = stringProperty()
    var mpkg: String? by mpkgProperty

    val modelOutputDirProperty = stringProperty()
    var modelOutputDir: String? by modelOutputDirProperty

    val projectNameProperty = stringProperty()
    var projectName: String by projectNameProperty

    val baseResourcesPathProperty = stringProperty()
    var baseResourcesPath: String by baseResourcesPathProperty

    val baseWebAppPathProperty = stringProperty()
    var baseWebAppPath: String by baseWebAppPathProperty

    val baseSrcPathProperty = stringProperty()
    var baseSrcPath: String by baseSrcPathProperty

    val settingFileProperty = stringProperty()
    var settingFile: String by settingFileProperty

    val projectPackageProperty = stringProperty()
    var projectPackage: String by projectPackageProperty

    val baseWEBINFPathProperty = stringProperty()
    var baseWEBINFPath: String by baseWEBINFPathProperty

    val removedTableNamePrefixesProperty = stringProperty()
    var removedTableNamePrefixes: String by removedTableNamePrefixesProperty

    val jdbcUrlProperty = stringProperty()
    var jdbcUrl: String? by jdbcUrlProperty

    val hostProperty = stringProperty()
    var host: String by hostProperty

    // 数据库端口
    val portProperty = stringProperty()
    var port: String by portProperty

    val dbtypeProperty = stringProperty()
    var dbtype: String by dbtypeProperty

    val dbnameProperty = stringProperty()
    var dbname: String by dbnameProperty

    val leftTableProperty = stringProperty()
    var leftTable: String by leftTableProperty

    val rightTableProperty = stringProperty()
    var rightTable: String by rightTableProperty

    val userProperty = stringProperty()
    var user: String? by userProperty

    val pwdProperty = stringProperty()
    var pwd: String? by pwdProperty

    val getTablesSqlProperty = stringProperty()
    var getTablesSql: String? by getTablesSqlProperty

    val tableCountsProperty = stringProperty()
    var tableCounts: String? by tableCountsProperty

    val remarkProperty = booleanProperty()
    var remark: Boolean? by remarkProperty

    val dbOKProperty = booleanProperty()
    var dbOK: Boolean? by dbOKProperty

    val dialectProperty = objectProperty<Dialect>()
    var dialect: Dialect? by dialectProperty

    val arpProperty = objectProperty<ActiveRecordPlugin>()
    var arp: ActiveRecordPlugin? by arpProperty

    val dataSourceProperty = objectProperty<DataSource>()
    var dataSource: DataSource? by dataSourceProperty

    val metaBuilderProperty = objectProperty<MetaBuilder>()
    var metaBuilder: MetaBuilder? by metaBuilderProperty

    val driverProperty = stringProperty()
    var driver: String? by driverProperty

    val baseTemplatePathProperty = stringProperty()
    var baseTemplatePath: String? by baseTemplatePathProperty


    val modelTemplateProperty = stringProperty()
    var modelTemplate: String? by modelTemplateProperty

    val engineProperty = objectProperty<Engine>()
    var engine: Engine by engineProperty

    val engineTemplateFilePathProperty = stringProperty()
    var engineTemplateFilePath: String? by engineTemplateFilePathProperty

    //    jfinal engine 模板字符串
    val tplStringProperty = stringProperty()
    var tplString: String by tplStringProperty

    //    jfinal engine 模板字符串渲染结果
    val tplStringOutProperty = stringProperty()
    var tplStringOut: String by tplStringOutProperty

    override fun updateModel(json: JsonObject) {
        with(json) {
            bmpkg = getString("bmpkg")
            bmpkgpath = string("bmpkgpath")
            mpkg = string("mpkg")
            modelOutputDir = string("modelOutputDir")
        }
    }

    override fun toJSON(json: JsonBuilder) = with(json) {
        add("projectName", projectName)
        super.toJSON(json)
    }

}

//数据库模型　ItemViewModel
class GmodelModel : ItemViewModel<Gmodel>() {
    val bmpkg = bind { item?.bmpkgProperty }
    val bmpkgpath = bind { item?.bmpkgpathProperty }
    val mpkg = bind { item?.mpkgProperty }
    val modelOutputDir = bind { item?.modelOutputDirProperty }
    val remark = bind { item?.remarkProperty }
    val jdbcUrl = bind { item?.jdbcUrlProperty }
    val host = bind { item?.hostProperty }
    val port = bind { item?.portProperty }
    val dbtype = bind { item?.dbtypeProperty }
    val dbname = bind { item?.dbnameProperty }
    val user = bind { item?.userProperty }
    val pwd = bind { item?.pwdProperty }
    val dbOK = bind { item?.dbOKProperty }
    val getTablesSql = bind { item?.getTablesSqlProperty }
    val dbs = FXCollections.observableArrayList("")
    //    与listview绑定时，增加删除时，FXCollections.observableArrayList会自动选择下一项，listProperty则不会
    val tables = FXCollections.observableArrayList("")
    val excludedTable = FXCollections.observableArrayList("")
    //    val tables = listProperty("")
//    val excludedTable = listProperty("")
    var dialect = bind { item?.dialectProperty }
    var driver = bind { item?.driverProperty }
    var dataSource = bind { item?.dataSourceProperty }
    var metaBuilder = bind { item?.metaBuilderProperty }
    var modelTemplate = bind { item?.modelTemplateProperty }
    var baseTemplatePath = bind { item?.baseTemplatePathProperty }
    var baseSrcPath = bind { item?.baseSrcPathProperty }
    var baseWebAppPath = bind { item?.baseWebAppPathProperty }
    var baseResourcesPath = bind { item?.baseResourcesPathProperty }
    var projectName = bind { item?.projectNameProperty }
    var projectPackage = bind { item?.projectPackageProperty }
    var tableCounts = bind { item?.tableCountsProperty }
    var baseWEBINFPath = bind { item?.baseWEBINFPathProperty }
    var removedTableNamePrefixes = bind { item?.removedTableNamePrefixesProperty }
    var settingFile = bind { item?.settingFileProperty }
    var leftTable = bind { item?.leftTableProperty }
    var rightTable = bind { item?.rightTableProperty }
    var engine = bind { item?.engineProperty }
    var tplString = bind { item?.tplStringProperty }
    var tplStringOut = bind { item?.tplStringOutProperty }
    var engineTemplateFilePath = bind { item?.engineTemplateFilePathProperty }
    var arp = bind { item?.arpProperty }

    init {
        this.tables.remove(0, 1)
        this.excludedTable.remove(0, 1)
        this.engine.value = Engine.use().addSharedMethod(StrKit()).setEncoding("utf-8")
                .setDevMode(true)
                .addSharedObject("gm",this)
                .addSharedObject("jsonUtil", JSONUtil())
                .addSharedObject("kv", Kv())
                .addSharedObject("hDbUtil", cn.hutool.db.DbUtil())
                .addSharedObject("jDb", Db())
                .addSharedObject("record", com.jfinal.plugin.activerecord.Record())
//                .addSharedObject("jDb", com.jfinal.plugin.activerecord.Db())
                .addDirective("now", NowDirective().javaClass)
                .addDirective("demo", Demo().javaClass)
        this.dbtype.value = "sqlite"
    }

}



